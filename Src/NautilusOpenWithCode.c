/*
    NautilusOpenWithCode
    Copyright (C) 2022 Zach Podbielniak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <string.h>
#include <nautilus-extension.h>
#include "NautilusOpenWithCode.h"


#define COMMAND_PREPEND NULL
/* #define COMMAND_PREPEND "distrobox-host-exec" */



/* Known Binary Names Of VsCode */
static const char *code_binaries[] = 
{
	"code",
	"code-oss"
};




struct _Nowc
{
	GObject 		parent_instance;
};




static 
void 
menu_provider_iface_init(
	NautilusMenuProviderInterface 		*iface
);




G_DEFINE_DYNAMIC_TYPE_EXTENDED(
	Nowc, 
	nowc, 
	G_TYPE_OBJECT, 
	0,
	G_IMPLEMENT_INTERFACE_DYNAMIC(
		NAUTILUS_TYPE_MENU_PROVIDER, 
		menu_provider_iface_init
	)
);




static
GString *
get_code_path(
	void
){
	int i;
	char *path;

	for (
		i = 0, path = NULL;
		i < G_N_ELEMENTS(code_binaries);
		i++
	){
		path = g_find_program_in_path(code_binaries[i]);

		if (NULL != path)
		{ goto __Found; }
		
	}

	__Found:
	if (NULL == path)
	{ return NULL; }

	return g_string_new(path);
}



static 
GString *
get_command_prepend(
	GString 					*code_path
){
	if (NULL != COMMAND_PREPEND)
	{
		char *path;
		GString *prepended;

		path = g_find_program_in_path(COMMAND_PREPEND);
		if (NULL != path)
		{
			prepended = g_string_new(NULL);
			g_string_printf(prepended, "%s \"%s\"", path, code_path->str);

			if (NULL != code_path)
			{ g_string_free(code_path, TRUE); }
			return prepended;
		}
	}

	return code_path;
}




static 
void
open_with_code_callback(
	NautilusMenuItem 			*item,
	gpointer				user_data
){
	GList *files;
	g_autoptr(GString) command = NULL;

	files = g_object_get_data(G_OBJECT(item), "files");
	command = get_code_path();
	command = get_command_prepend(command);

	if (NULL == command)
	{
		g_print("CODE PATH IS NULL!");
		return; 
	}

	/* Open this in a new window everytime! */
	g_string_append(command, " --new-window");

	for (GList *l = files; l != NULL; l = l->next)
	{
		char *uri = NULL;
		char *path = NULL;

		uri = nautilus_file_info_get_uri(l->data);
	
		if (NULL != uri)
		{
			path = g_filename_from_uri(
				uri,
				NULL,
				NULL
			);

			g_print("Uri: %s\n", uri);
			g_print("Path: %s\n", path);

			g_string_append_printf(command, " \"%s\"", path);
			g_free(path);
		}

	}

	g_print("Running following command \"%s\"\n", command->str);
	g_spawn_command_line_async(command->str, NULL);
}




static 
void
open_with_code_callback_background(
	NautilusMenuItem 			*item,
	gpointer				user_data
){
	if (NULL == user_data)
	{ return; }

	NautilusFileInfo *current_folder;
	g_autoptr(GString) command = NULL;
	char *uri = NULL;
	char *path = NULL;

	current_folder = user_data;
	
	command = get_code_path();

	if (NULL == command)
	{
		g_print("CODE PATH IS NULL!");
		return; 
	}

	/* Open this in a new window everytime! */
	g_string_append(command, " --new-window");


	uri = nautilus_file_info_get_uri(current_folder);
	g_print("Uri: %s\n", uri);

	/* Expecting uri to start with "file://", so lets remove */
	if (NULL != uri)
	{ uri = (char *)((size_t)uri + strlen("file///")); }

	g_print("Path: %s\n", uri);

	g_string_append_printf(command, " \"%s\"", uri);

	g_print("Running following command \"%s\"\n", command->str);
	g_spawn_command_line_async(command->str, NULL);
}





static 
GList *
get_file_items(
	NautilusMenuProvider 			*provider,
	GList                			*files
){
	GList *items = NULL;
	gboolean one_item;
	NautilusMenuItem *item;
	Nowc *nowc;

	nowc = NAUTILUS_NOWC(provider);

	if (files == NULL)
	{ return NULL; }

	one_item = (files != NULL) && (files->next == NULL);
	if (one_item && !nautilus_file_info_is_directory((NautilusFileInfo *)files->data))
	{
		item = nautilus_menu_item_new(
			"Nowc::OpenWithCode",
			"Open File With Code",
			"Open File With Visual Studio Code",
			NULL
		);
	}
	/* More than one file but not a directory */
	else if (!nautilus_file_info_is_directory((NautilusFileInfo *)files->data))
	{
		item = nautilus_menu_item_new(
			"Nowc::OpenWithCode",
			"Open File(s) With Code",
			"Open File(s) With Visual Studio Code",
			NULL
		);
	}
	else
	{
		item = nautilus_menu_item_new(
			"Nowc::OpenWithCode",
			"Open Directory With Code",
			"Open Directory With Visual Studio Code",
			NULL
		);
	}
	
	g_signal_connect(
		item,
		"activate",
		G_CALLBACK(open_with_code_callback),
		provider
	);
 
 	g_object_set_data_full(
		G_OBJECT(item),
		"files",
		nautilus_file_info_list_copy(files),
		(GDestroyNotify)nautilus_file_info_list_free
	);

	items = g_list_append(items, item);
	return items;
}




GList *
get_background_items(
	NautilusMenuProvider			*provider,
	GtkWidget				*window,
	NautilusFileInfo			*current_folder
){
	GList *items = NULL;
	gboolean one_item;
	NautilusMenuItem *item;
	Nowc *nowc;

	nowc = NAUTILUS_NOWC(provider);

	if (current_folder == NULL)
	{ return NULL; }

	if (!nautilus_file_info_is_directory(current_folder))
	{
		g_print("Folder Invoked For Not A Folder!\n");
		return NULL;
	}
	else
	{
		item = nautilus_menu_item_new(
			"Nowc::OpenWithCode",
			"Open Directory With Code",
			"Open Directory With Visual Studio Code",
			NULL
		);
	}
	g_signal_connect(
		item,
		"activate",
		G_CALLBACK(open_with_code_callback_background),
		current_folder
	);
 
	items = g_list_append(items, item);
	return items;
}




static 
void
menu_provider_iface_init(
	NautilusMenuProviderInterface 		*iface
){ 
	iface->get_file_items = get_file_items; 
	/* iface->get_background_items = get_background_items; */
}




static 
void
nowc_init(
	Nowc 					*nowc
){ (void)0; }




static 
void
nowc_class_init(
	NowcClass 				*klass
){ (void)0; }




static 
void
nowc_class_finalize(
	NowcClass 				*klass
){ (void)0; }




void
nowc_load(
	GTypeModule 				*module
){ nowc_register_type(module); }
