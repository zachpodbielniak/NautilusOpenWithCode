#   NautilusOpenWithCode
#   Copyright (C) 2022 Zach Podbielniak

#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published
#   by the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.

#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

CC := gcc
CFLAGS := -fpic -pthread -O0
CFLAGS += $(shell pkg-config glib-2.0 --cflags --libs)
CFLAGS += $(shell pkg-config libnautilus-extension-4 --cflags --libs)
CFLAGS += $(shell pkg-config gtk4 --cflags --libs)

objects := NautilusEntryPoint.o NautilusOpenWithCode.o

.PHONY: all build clean rm install bear build_container push_container build_build push_build build_image

all: bin build
	echo $(CFLAGS)

bin:
	mkdir -p bin/

build: $(objects)
	$(CC) -shared $(addprefix bin/,$^) -o bin/libnautilus-open-with-code.so $(CFLAGS)

$(objects): %.o: Src/%.c
	gcc -c $^ -o $(addprefix bin/,$@) $(CFLAGS)

clean:
	rm -rf ./bin


# --------------------------------------------------------------------------------------


rm:
	sudo rm /usr/lib/x86_64-linux-gnu/nautilus/extensions-3.0/libnautilus-open-with-code.so

install:
	sudo cp bin/libnautilus-open-with-code.so /usr/lib64/nautilus/extensions-4/

bear:
	bear -- make all


build_container:
	podman build --no-cache -t quay.io/zachpodbielniak/nautilusopenwithcode:40 -f ./Containerfile


push_container:
	podman push quay.io/zachpodbielniak/nautilusopenwithcode:40


build_build:
	podman build --no-cache -t quay.io/zachpodbielniak/nautilusopenwithcode:build -f ./build/Containerfile ./build

push_build:
	podman push quay.io/zachpodbielniak/nautilusopenwithcode:build 

MANIFEST := now
PLATFORM := linux/amd64,linux/arm64
REGISTRY := quay.io
VERSION := 40

build_image:
	buildah manifest rm $(MANIFEST) || true 
	buildah manifest create $(MANIFEST)
	buildah build \
		--jobs=2 \
		--platform=$(PLATFORM) \
	    --manifest $(MANIFEST) \
		--no-cache \
		-t $(REGISTRY)/zachpodbielniak/nautilusopenwithcode:$(VERSION) \
		-f ./Containerfile \
		--build-arg=FEDORA_VERSION=$(VERSION) \
	    .
	
	buildah manifest push --all $(MANIFEST) docker://$(REGISTRY)/zachpodbielniak/nautilusopenwithcode:$(VERSION)

