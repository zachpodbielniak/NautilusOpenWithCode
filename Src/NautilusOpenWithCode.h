/*
    NautilusOpenWithCode
    Copyright (C) 2022 Zach Podbielniak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef NAUTILUSOPENWITHCODE_H
#define NAUTILUSOPENWITHCODE_H

#include <glib-object.h>
#include <gtk/gtk.h>
G_BEGIN_DECLS


#define NAUTILUS_TYPE_NOWC 	(nowc_get_type())


G_DECLARE_FINAL_TYPE(Nowc, nowc, NAUTILUS, NOWC, GObject)




void
nowc_load(
	GTypeModule			*module
);


G_END_DECLS
#endif