# NautilusOpenWithCode

A nautilus file manager extension that brings the option to "open with code" on the right click menu for a file or directory. Similar to the one provided with Windows out of the box.


## License
![AGPLv3](https://www.gnu.org/graphics/agplv3-with-text-162x68.png)
```
    NautilusOpenWithCode
    Copyright (C) 2022 Zach Podbielniak

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```

## Donate
Like NautilusOpenWithCode? You use it yourself? Why not donate to help make it better! I really appreciate any and all donations.

+ [PayPal](https://paypal.me/ZPodbielniak)
+ **Bitcoin** - 3C6Fc9WPH54GoVC91Sq4JTWa5C9ijKMA23
+ **Litecoin** - MVjvGjfmp3gkLniBSnreFb3SNEXq1FRxbW
+ **Ethereum** - 0xE58bAEd820308038092F732151c162f530361B59


## Installation
### Fedora
```
dnf install nautilus-devel nautilus-extensions gtk4-devel
```