#!/bin/bash
MANIFEST="nowc"
PLATFORM="linux/amd64,linux/arm64"

git clone https://gitlab.com/zachpodbielniak/NautilusOpenWithCode.git

buildah login $REGISTRY -u $USER -p $PASS
[ $? -ne 0 ] && exit 1

buildah manifest rm $MANIFEST || true 
buildah manifest create $MANIFEST
buildah build \
	--jobs=2 \
	--platform=$PLATFORM \
    --manifest $MANIFEST \
	--no-cache \
	-t $REGISTRY/zachpodbielniak/nautilusopenwithcode:$VERSION \
	-f ./NautilusOpenWithCode/Containerfile \
	--build-arg=FEDORA_VERSION=$VERSION \
    ./NautilusOpenWithCode

[ $? -ne 0 ] && exit 1

buildah manifest push --all $MANIFEST docker://$REGISTRY/zachpodbielniak/nautilusopenwithcode:$VERSION
[ $? -ne 0 ] && exit 1

# if [ "$SET_AS_LATEST" == "1" ]  
# then
# 	buildah tag "$REGISTRY/zachpodbielniak/nautilusopenwithcode:$VERSION" "$REGISTRY/zachpodbielniak/nautilusopenwithcode:latest"
# 	[ $? -ne 0 ] && exit 1
#
# 	buildah push "$REGISTRY/zachpodbielniak/nautilusopenwithcode:latest"
# 	[ $? -ne 0 ] && exit 1
# fi

exit 0
